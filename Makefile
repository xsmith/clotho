.DEFAULT_GOAL: default
.PHONY: default install remove

PACKAGE := clotho

# The default action is to install the package.
default: install

# Install this package and register it with raco.
install:
	raco pkg install --auto $(PACKAGE)/

# Attempt to remove the package. This will fail if there are packages that
# require $(PACKAGE) as a dependency.
remove:
	@raco pkg remove --auto $(PACKAGE) \
	  || (echo "\nConsider removing the dependent packages listed above by doing:" \
	           "\n    raco pkg remove --auto <package>" \
	           "\nYou can also force the removal to proceed by doing:" \
	           "\n    raco pkg remove --force $(PACKAGE)" \
	           "\nbut this is not recommended.\n" \
	           1>&2 ; \
	      exit 1)
