#lang scribble/manual
@; -*- mode: Scribble -*-

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@(require racket/runtime-path
          racket/file
          racket/string
          scribble/core
          (for-syntax racket/base syntax/parse)
          (for-label clotho))

@; Like `@racketmod`, but reads the code from a separate file.
@; Arg `filename` is the caption in title of the enclosing box.
@; Arg `runtime-path` is the path of the file to be read.
@(define (include-example filename runtime-path)
  (nested-flow
   (style 'code-inset '())
   (list
    (filebox
     filename
     (typeset-code
      #:context #'here
      #:keep-lang-line? #t
      #:indent 0
      (string-trim (file->string runtime-path)))))))

@; Since all the example files are in the "../examples/guide/" directory,
@; let's make @include-guide-example{foo.rkt} automatically pull in
@; "../examples/guide/foo.rkt", but only display "foo.rkt" for the file name.
@(define-syntax (include-guide-example stx)
   (syntax-parse stx
     [(_ filename)
      #'(begin
          (define-runtime-path rtp (build-path "../examples/guide/" filename))
          (include-example filename rtp))]))

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@title[#:tag "sec:guide"]{How to Use Clotho}
@author[(@author+email "Pierce Darragh" "pierce.darragh@gmail.com")
        (@author+email "William Gallard Hatch" "william@hatch.uno")
        (@author+email "Eric Eide" "eeide@cs.utah.edu")]

Clotho is a library that provides new randomness primitives that enable the
parameterization of random value generation.

@section{About Clotho}

Clotho is a library for supporting @emph{parametric randomness}.
Parametric randomness is a type of random value generation that allows you to
write programs that use random values, record those values, manipulate them
externally, and then use them to guide a subsequent execution of your program.

In addition to support for parametric randomness, Clotho provides almost all of
the randomness functionality of the Racket standard library---meaning you can
use Clotho any time you want random values.

Clotho also provides the ability to embed externally defined functions
(such as functions that depend on using Racket's built-in randomness
functions).
This allows for using parametric randomness with practically any existing
library, which is good news for you, since you won't have to re-implement them
from scratch.

@section{Getting Started}

In this section, we go over the basics of Clotho.

@subsection{Installation}

To install Clotho, you need a modern version of
@hyperlink["http://racket-lang.org"]{Racket}. (We recommend version 7.0 or
later.)
Use Racket's built-in @tt{raco} tool to install Clotho:

@verbatim|{$ raco pkg install clotho}|

@margin-note{
  This guide shows verbatim code blocks such as the one immediately above this
  note to indicate interactions on the command line.
  Within these blocks, a leading @racketid[$] indicates a line the user could
  type at a shell, and a leading @racketid[>] indicates a line the user could
  type within a Racket REPL session.
  Lines that do not begin with either of these symbols represent output.
}

@subsection{Enabling Clotho in Your Code}

A Clotho program will typically start like this:

@racketmod[
clotho
]

But you can also do:

@racketmod[
racket

(require clotho)
]

Throughout this guide, we will predominantly be using Clotho as a
@hash-lang[], but the examples should mostly work identically if you instead
@racket[require] it.

@section{The Basics of Clotho}

Clotho provides a set of randomness primitives and convenience functions that
serve two purposes:

@itemlist[
#:style 'ordered
@item{Replace randomness functionality of @racket[racket/base].}
@item{Enable parametric randomness.}
]

Let's learn about how to use Clotho to achieve these two goals.

@subsection{The @racket[current-random-source] Parameter}

Crucially, all of Clotho's functionality revolves around the
@racket[current-random-source] parameter.

@margin-note{
  If you do not know about @emph{parameters} in Racket, we recommend you take a
  moment to consult
  @hyperlink["https://docs.racket-lang.org/reference/parameters.html"]{the
  documentation about them}.
}

The @racket[current-random-source] must be parameterized with a new
@racket[random-source?], which can easily be constructed by calling
@racket[make-random-source].
The @racket[make-random-source] function is fairly versatile.
You can supply it with:

@itemlist[
@item{No argument, creating a random @racket[random-source?].}
@item{An integer argument, which will be used as a seed value from which to
generate new random values.}
@item{A @racket[bytes?] argument, which will be used to (a) ``replay'' the values
it encodes and (b) generate new values when the recording is used up.}
]

The randomness primitives and convenience functions will only work where the
@racket[current-random-source] is parameterized.
For example:

@racketmod[
#:file "success.rkt"
clotho

(parameterize ([current-random-source (make-random-source)])
  (println (random)))
]

@margin-note{
  All of the code samples in the Clotho guide are available in the Clotho
  repository, under the
  @hyperlink["https://gitlab.flux.utah.edu/xsmith/clotho/-/tree/master/clotho/examples/guide"]{@racket[clotho/examples/guide]}
  directory.
  For example, the above code sample can be found in the
  @hyperlink["https://gitlab.flux.utah.edu/xsmith/clotho/-/tree/master/clotho/examples/guide/success.rkt"]{@racket[clotho/examples/guide/success.rkt]}
  file.
}

@verbatim|{
  $ racket success.rkt
  0.9193169593852777
}|

Outside of the body of such a @racket[parameterize] expression, an error will
be thrown instead:

@racketmod[
#:file "error.rkt"
clotho

(println (random))
]

@verbatim|{
  $ racket error.rkt
  assert-random-source-initialized: current-random-source not yet initialized!
    Have you `parameterize`d it?
}|

@subsection{Extracting the Byte String}

After calling some randomness functions, we can analyze the recording of the
generation that was captured in the @racket[current-random-source].
This is a byte string (@racket[bytes?]), which can be obtained by calling the
@racket[get-current-random-source-byte-string] function:

@(include-guide-example "byte-string.rkt")

@verbatim|{
  $ racket byte-string.rkt
  -518059394
  #";\240)`\355\325\334\354\31\344\310\244"
  '(59 160 41 96 237 213 220 236 25 228 200 164)
}|

From this output, we know the first call to @racket[random-bool] produced
@racket[#t], and then the call to @racket[random-int] produced
@racket[-518059394].

The byte string captured in @racket[bs] consists of 4-byte chunks that each
represent an integer.
The first integer is reserved for internal use.
Each 4-byte integer after that corresponds directly to the output of a
randomness function.
Since there were two calls to randomness functions, we see 12 bytes in the byte
string: 4 for internal use, and then 2 x 4 = 8 bytes devoted to randomness
calls.

@subsection{Replaying Randomness with Changes}

What makes Clotho neat is the ability to run a random program a second time and
@emph{replay} the values obtained the first time.
For example, we can rewrite the above program to use the previously generated
byte string, and then execute it:

@include-guide-example{replay.rkt}

@verbatim|{
  $ racket replay.rkt
  -518059394
  #";\240)`\355\325\334\354\31\344\310\244"
  '(59 160 41 96 237 213 220 236 25 228 200 164)
}|

By providing the byte string as an argument to @racket[make-random-source], we
create a new @racket[random-source?] that will produce the same randomness
values we got out previously.

This isn't very exciting, though.
What would be @emph{really} cool is if we could manipulate the source of
randomness to induce subtle changes in the generated values.
As you probably guessed by the way that last sentence was
written, we can!

Let's manipulate the integer that corresponds to the call that produced
@racket[#t] in the previous program, and then feed the manipulated byte string
into the same program to see what happens:

@include-guide-example{manipulate.rkt}

@verbatim|{
  $ racket manipulate.rkt
  #";\240)`\355\325\334\355\31\344\310\244"
  '(59 160 41 96 237 213 220 237 25 228 200 164)
  #t
  #";\240)`\355\325\334\355\31\344\310\244"
  '(59 160 41 96 237 213 220 237 25 228 200 164)
}|

We can see that we incremented the seventh byte by 1, which we expect to modify
the value of the first randomness function.
After the generation is complete, we get back the same byte string we passed
in.
This is because the byte string was sufficiently long to handle all of the
needed randomness calls.

@margin-note{
  It is because each of these 4-byte segments can be handled separately from
  one another that we say Clotho provides mechanisms for @emph{parametric
  randomness}.
  Essentially, each 4-byte segment becomes a @emph{parameter} that can be
  adjusted.
}

The output we get out is the value @racket[#t], instead of the number
@racket[-518059394] that we had received previously.
This shows that the first randomness call (the call to @racket[random-bool] in
the condition of the @racket[if] expression) output a @racket[#f] instead of
the @racket[#t] we were getting before.

Note that the third tetrad of bytes in the byte string (i.e., bytes 8--11) did
not change, but produced a boolean instead of an integer value.
This is because Clotho's byte strings do not record values directly, but rather
record numbers that are seeds to individual random generators that
are used for producing values for each randomness call.
Although this reduces Clotho's efficiency somewhat, it increases the expressive
power considerably.

@section{Advanced Clotho Usage}

There are some additional aspects of Clotho's design that can be very helpful
to know, but that are somewhat more advanced than what we've covered so far.

@subsection{Using Externally Defined Randomness Functions}

Sometimes, you may find yourself wanting to use a function somebody else has
written that uses Racket's built-in randomness functions.
This could be problematic if you want calls to this function to be recorded in
the @racket[current-random-source]'s byte string.

Clotho provides a form, @racket[wrap-external-randomness], that allows you to
achieve exactly this.
Consider this example using the @racket[shuffle] function provided by the
@racket[racket/list] library:

@include-guide-example{external.rkt}

Then, we run the code @emph{twice}:

@verbatim|{
  $ racket external.rkt
  '(4 9 2 5 0 8 7 6 3 1)
  #"\0\0\0\0"
  '(0 0 0 0)

  '(5 7 0 2 9 6 8 1 3 4)
  #"\0\0\0\0\347\240n\333"
  '(0 0 0 0 231 160 110 219)

  $ racket external.rkt
  '(0 2 5 1 4 3 9 8 6 7)
  #"\0\0\0\0"
  '(0 0 0 0)

  '(5 7 0 2 9 6 8 1 3 4)
  #"\0\0\0\0\347\240n\333"
  '(0 0 0 0 231 160 110 219)
}|

There are a few things to notice about the outputs here.

First, the return values @racket[rv1] and @racket[rv2] are not the same within
each execution.
Second, the @racket[rv2] values are identical between executions, but the
@racket[rv1] values are not.
Third, the byte strings labeled @racket[bs1] are shorter than the byte strings
labeled @racket[bs2].

The takeaway here is that the @racket[wrap-external-randomness] form has caused
the calls to @racket[shuffle]---which is not implemented with Clotho's
randomness functions---to become constrained by the
@racket[current-random-source].

The @racket[rv1] values in each execution are unpredictable, despite occurring
within the parameterization of the @racket[current-random-source].
In most cases, this would probably be an undesirable effect.
In contrast, the @racket[rv2] values are correctly handled by the
@racket[current-random-source].
In both executions, @racket[rv2] and @racket[bs2] come back the same.
Additionally, we see that the byte string increased in length.
This is because Clotho generated and recorded a randomness value associated
with the wrapped call to @racket[shuffle], which did not happen in the call
that wasn't wrapped with @racket[wrap-external-randomness].

It is important to point out that Clotho is unable to wrap each individual call
to a randomness function within the scope of a
@racket[wrap-external-randomness] form.
Rather, the execution of the entire form is associated with exactly one 4-byte
segment of the byte string.
To put it in code:

@include-guide-example{external-multiple.rkt}

@verbatim|{
  $ racket external-multiple.rkt
  #"\0\0\0\0\347\240n\333"
  #"\0\0\0\0\347\240n\333l\331\317\250"
}|

The first byte string, @racket[bs1], shows that only one 4-byte segment was
created for the two calls to @racket[random].
On the other hand, @racket[bs2] is 4 bytes longer than @racket[bs1].
This is because each call to @racket[wrapped-shuffle] uses its own
@racket[wrap-external-randomness] form.
The advantage of this is that each randomness call is now parameterized (in the
Clotho sense of the word).

In most cases, it will be more advantageous to opt for very tight scoping on
usages of @racket[wrap-external-randomness].
This requires either wrapping each individual call to a randomness function, or
else defining a new wrapper function to handle the wrapping more easily.

@subsection{Abstracting Regions of Randomness}

It can occasionally be useful to abstract complex uses of randomness functions.
For example, you may not be interested in parameterizing each randomness
function, but rather a broader-scoped meta-randomness function:

@include-guide-example{racket-abstract.rkt}

@verbatim|{
  $ racket abstract.rkt
  '(-826043031 #t #\g)
  #"\0\0\0\0\347\240n\333l\331\317\250.\313\2454"
  '(0 0 0 0 231 160 110 219 108 217 207 168 46 203 165 52)

  '(-1378798560 #f #\h)
  #"\0\0\0\0\347\240n\333"
  '(0 0 0 0 231 160 110 219)
}|

We can quickly observe that the return values @racket[rv1] and @racket[rv2] are
different.
More interesting in the present discussion, however, is the difference in
lengths of the byte strings @racket[bs1] and @racket[bs2].
There are 16 bytes in @racket[bs1], versus only 8 bytes in @racket[bs2].
This tells us that @racket[bs1] corresponds to three randomness function calls,
while @racket[bs2] corresponds to only one.

The @racket[with-new-random-source] form used in
@racket[abstracted-random-list-of-vals] makes it so that all of the Clotho
randomness functions called within its body will correspond to a single 4-byte
segment of the output byte string.

@margin-note{
  The @racket[with-new-random-source] form is a macro that automatically
  creates a new @racket[current-random-source] seeded with the value returned
  from calling @racket[random-seed-value] within the context of the
  @emph{parent} @racket[current-random-source].
  This means that if you call @racket[get-current-random-source-byte-string]
  within the body of the @racket[with-new-random-source] form, it will return
  the byte string of the @emph{child} @racket[current-random-source]---which
  is probably not what you intended.
  Be careful where you call @racket[get-current-random-source-byte-string]!
}

@subsection{Easy Stateful Execution}

There are times when you may want to use randomness functions without having to
use a cumbersome @racket[parameterize] expression.
For example, if you are interacting with the Racket REPL to test some
functionality, it can be frustrating to wrap each call in this way.
To accommodate this, you can @racket[(require clotho/stateful)]:

@verbatim|{
  $ racket
  Welcome to Racket v7.4.
  > (require clotho)
  > (random)
  ; assert-random-source-initialized: current-random-source not yet initialized!
  ;   Have you `parameterize`d it? [,bt for context]
  > (require clotho/stateful)
  > (random)
  0.3642047298500743
}|

There are various forms to support the use of this mode documented in the
@Secref["sec:api"].
Note also that @racket[clotho/stateful] can be used as a @hash-lang[]:

@racketmod[
clotho/stateful
]

@subsection{Automatically Wrapping a Whole Module}

When you wish to use an external randomness function from a given module, you
can use Clotho's @racket[wrap-external-randomness] macro to wrap the function
call in such a way that the internal randomness will be guided by Clotho like
any other randomness call in Clotho.
However, this can be cumbersome to use if you wish to wrap many functions
within a library.

To address this issue, Clotho provides the @racket[require-clotho-wrapped]
macro that can be used in a @racket[require] position.
This macro will automatically wrap every Phase 0 binding within that module
with @racket[wrap-external-randomness].

For example, we can wrap the @racket[math/distributions] library since it
provides quite a few randomness functions, and it's reasonable to want to use
many of them at once:

@include-guide-example{math-distributions.rkt}

@verbatim|{
  $ racket math-distributions.rkt
  '(-1.4422649979023663 0.170764610196471 -0.014599159848523677)
  #"\360\343+g\356\344\207\225\217\307_\325"
  '(240 227 43 103 238 228 135 149 143 199 95 213)

  '(-1.4422649979023663 0.170764610196471 -0.014599159848523677)
  #"\360\343+g\356\344\207\225\217\307_\325"
  '(240 227 43 103 238 228 135 149 143 199 95 213)
}|

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@; End of file.
