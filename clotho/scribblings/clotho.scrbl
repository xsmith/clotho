#lang scribble/manual
@; -*- mode: Scribble -*-

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@title{Clotho}
@author[(@author+email "Pierce Darragh" "pierce.darragh@gmail.com")
        (@author+email "William Gallard Hatch" "william@hatch.uno")
        (@author+email "Eric Eide" "eeide@cs.utah.edu")]

Clotho is a Racket library that supports parametric
randomness in software applications.
@;
@Secref["sec:guide"] explains how to install and use Clotho, and
@Secref["sec:api"] provides the documentation for the API.

@table-of-contents[]
@include-section["guide.scrbl"]
@include-section["api.scrbl"]
@include-section["acknowledgments.scrbl"]
@include-section["code-and-license.scrbl"]

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@; End of file.
