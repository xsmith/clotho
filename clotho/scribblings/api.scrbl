#lang scribble/manual
@; -*- mode: Scribble -*-

@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@require[@for-label[clotho/racket/base
                    clotho
                    (prefix-in racket/base: racket/base)
                    racket/contract]]

@title[#:tag "sec:api"]{Clotho API}
@author[(@author+email "Pierce Darragh" "pierce.darragh@gmail.com")
        (@author+email "William Gallard Hatch" "william@hatch.uno")
        (@author+email "Eric Eide" "eeide@cs.utah.edu")]

@defmodule[clotho]

Clotho is a library that gives Racket support for parametric randomness.
We define @italic{parametric randomness} as a specific variety of random
generation wherein the individual random choices can be externally manipulated
by tuning ``parameters.''
A fuller definition is given below.  @; TODO: add definition

@section{Source of Randomness}

Clotho uses a specially designed random source to allow for easy manipulation
of future random generations.
This source, @racket[current-random-source], must be parameterized whenever a
Clotho randomness function is called, or else an error will occur.
To create a random source, the function @racket[make-random-source] can be
invoked during parameterization.

@defparam[current-random-source random-source random-source?]{
A parameter for defining the current source of deterministic randomness.
}

@defproc[(random-source? [v any/c]) boolean?]{
Checks whether a value is suitable for being used as the
@racket[current-random-source].
}

@defproc[(make-random-source [v any/c]) random-source?]{
Creates a @racket[random-source?], which can be used to parameterize
@racket[current-random-source].
The @racket[v] can be any of:

@itemlist[
  @item{@racket[#f] -- generates a random source free of any manipulation}
  @item{@racket[bytes?] -- uses a byte sequence}
  @item{@racket[(integer-in 0 (sub1 (expt 2 31)))] -- use @racket[v] as a
  seed value}
]
}

@defproc[(current-random-source-initialized?) boolean?]{
Checks whether the @racket[current-random-source] has been initialized.
}

@defproc[(assert-current-random-source-initialized!) void?]{
Raises an error if @racket[current-random-source-initialized?] would return
@racket[#f].
}

@defproc[(get-current-random-source-byte-string) bytes?]{
Returns an immutable byte string which records all randomness generations that
have happened and all those which will happen (if they have not yet come to
pass).
}

@defproc[(get-next-uint-from-current-random-source!)
         (integer-in 0 (- (expt 2 32) 209))]{
Forces the @racket[current-random-source] to produce an unsigned integer.
}

@subsection{Example of Random Source Parameterization}

A typical way to begin invoking Clotho's randomness functions might look like:

@racketblock[
(require clotho)

(code:comment "Let's seed the source with the value 42.")
(parameterize ([current-random-source (make-random-source 42)])
  (random))  (code:comment "=> 0.5400850091450108")
]

@section{Random Generation Functions}

These functions can be used within the context of the
@racket[current-random-source] to produce pseudo-random values.
All of these functions are implemented in terms of @racket[random]:

@defproc*[([(random) exact-nonnegative-integer?]
           [(random [k (integer-in 1 4294967087)]) exact-nonnegative-integer?]
           [(random [min exact-integer?] [max (integer-in (+ 1 min)
                                                          (+ 4294967087 min))])
            exact-nonnegative-integer?])]{
Identical to Racket's @racket[racket/base:random] function, but uses Clotho's randomness
source.
}

@subsection{Common}

These are the most commonly used randomness functions.
They provide facilities for generating Booleans, integers, and randomly
selecting elements from a sequence.

@defproc[(random-bool) boolean?]{
Returns a random Boolean value.
}

@defproc[(random-uint) (integer-in 0 (- (expt 2 32) 209))]{
Returns a random unsigned integer.
}

@defproc[(random-int)
         (integer-in (- (- (expt 2 31) 104)) (- (expt 2 31) 105))]{
Returns a random signed integer.
}

@defproc[(random-seed-value) (integer-in 0 (sub1 (expt 2 31)))]{
Returns a random unsigned integer value suitable for use as a seed (such as for
@racket[make-random-source]).
}

@defproc[(random-ref [seq sequence?]) any/c]{
Returns a random element of the sequence.
}

@defproc[(random-in-bound-lists [bound-lists (listof (list/c integer? integer?))]) integer?]{
Takes a list containing boundary lists, where each boundary list is a list of a low and high number valid for use in @racket[random], where the boundary includes the low number and excludes the high number.
The result is a random number drawn evenly from the various bounds.

In other words the following example with return with equal probability any number in the list @racket[(list 1 2 3 4 101 102)]:

@racketblock[
(random-in-bound-lists (list (list 1 5)
                             (list 101 103)))
]

}

@subsection{Characters}

In addition to the simple randomness functions, Clotho provides functions for
randomly generating values from certain character classes.

@defproc[(random-char) char?]{
Returns a random Unicode character.
The bytecode ranges used are 0-55295 and 57344-1114111.
}

@defproc*[([(random-char-in-range [seq sequence?]) char?]
           [(random-char-in-range [low integer?]
                                  [high integer?]) char?])]{
Returns a character corresponding to a randomly selected integer within the bounds or in the sequence given.

When given bounds, as with the @racket[random] function, the bounds include the low value and exclude the high value.

The easiest way to use this with a sequence is with the @racket[range] function, as in:

@racketblock[
(random-char-in-range (range 3 17))
]
}

@defproc[(random-char-in-bound-lists [bound-lists (listof (list/c integer? integer?))]) char?]{
Like @racket[random-in-bound-lists], but converts the result into a character.
}

@defproc[(random-ascii-char) char?]{
Returns a random ASCII character, including control characters and null characters.
}

@defproc[(random-ascii-lower-char) char?]{
Returns a random ASCII lowercase character (bytecode 97-122; regex [a-z]).
}

@defproc[(random-ascii-upper-char) char?]{
Returns a random ASCII uppercase character (bytecode 65-90; regex [A-Z]).
}

@defproc[(random-ascii-alpha-char) char?]{
Returns a random ASCII alphabetical character (bytecode 97-122 or 65-90; regex
[a-zA-Z]).
This range contains @racket[random-ascii-lower-char] and
@racket[random-ascii-upper-char].
}

@defproc[(random-ascii-numeral-char) char?]{
Returns a random ASCII numerical character (bytecode 48-57; regex [0-9]).
}

@defproc[(random-ascii-alphanumeric-char) char?]{
Returns a random ASCII alphanumerical character (bytecode 97-122, 65-90, or
48-57; regex [a-zA-Z0-9]).
This range contains both @racket[random-ascii-alpha-char] and
@racket[random-ascii-numeral-char].
}

@defproc[(random-ascii-word-char) char?]{
Returns a random ASCII word character (bytecode 97-122, 65-90, 48-57, or 95;
regex [a-zA-Z0-9_]).
This range contains @racket[random-ascii-alphanumeric-char] as well as the
underscore, @racket[#\_].
}

@subsection{Strings}

Convenience functions are also provided for generating random strings using the
character generation functions of the previous section.

@defproc[(random-string-from-char-producing-proc
          [proc (-> char?)]
          [bound (or/c exact-nonnegative-integer? #f) #f]
          [pre-proc (or/c (-> char?) #f) #f]
          [post-proc (or/c (-> char?) #f) #f])
         string?]{
Returns a random string consisting of characters drawn from @racket[proc].
The string will be of a random length no greater than @racket[bound].
If the @racket[bound] is @racket[#f], the maximum length of the string is 128.
@; TODO: maybe the bound should be a parameter

If the @racket[pre-proc] character-producing function is given, the first
character of the string will be produced with it.
The remainder of the string will be no greater than @racket[(- bound 1)] in
length.

If the @racket[post-proc] character-producing function is given, the last
character of the string will be produced with it.
The previous portion of the string will be no greater than @racket[(- bound 1)]
in length.

(If both @racket[pre-proc] and @racket[post-proc] are given, the interior
portion of the string will be no greater than @racket[(- bound 2)] in length.)
}

@defproc[(random-string [bound (or/c exact-nonnegative-integer? #f) #f])
         string?]{
Returns a random string of characters with length no greater than
@racket[bound].
The characters are generated by calling @racket[random-char].

If the @racket[bound] is @racket[#f], the maximum length of the string is 128.
}

@defproc[(random-ascii-lower-string
          [bound (or/c exact-nonnegative-integer? #f) #f]) string?]{
Returns a random string of characters with length no greater than
@racket[bound].
The characters are generated by calling @racket[random-ascii-lower-char].

If the @racket[bound] is @racket[#f], the maximum length of the string is 128.
}

@defproc[(random-ascii-upper-string
          [bound (or/c exact-nonnegative-integer? #f) #f]) string?]{
Returns a random string of characters with length no greater than
@racket[bound].
The characters are generated by calling @racket[random-ascii-upper-char].

If the @racket[bound] is @racket[#f], the maximum length of the string is 128.
}

@defproc[(random-ascii-alpha-string
          [bound (or/c exact-nonnegative-integer? #f) #f]) string?]{
Returns a random string of characters with length no greater than
@racket[bound].
The characters are generated by calling @racket[random-ascii-alpha-char].

If the @racket[bound] is @racket[#f], the maximum length of the string is 128.
}

@defproc[(random-ascii-numeral-string
          [bound (or/c exact-nonnegative-integer? #f) #f]) string?]{
Returns a random string of characters with length no greater than
@racket[bound].
The characters are generated by calling @racket[random-ascii-numeral-char].

If the @racket[bound] is @racket[#f], the maximum length of the string is 128.
}

@defproc[(random-ascii-alphanumeric-string
          [bound (or/c exact-nonnegative-integer? #f) #f]) string?]{
Returns a random string of characters with length no greater than
@racket[bound].
The characters are generated by calling
@racket[random-ascii-alphanumeric-char].

If the @racket[bound] is @racket[#f], the maximum length of the string is 128.
}

@defproc[(random-ascii-word-string
          [bound (or/c exact-nonnegative-integer? #f) #f]) string?]{
Returns a random string of characters with length no greater than
@racket[bound].
The characters are generated by calling @racket[random-ascii-word-char].

If the @racket[bound] is @racket[#f], the maximum length of the string is 128.
}

@subsection{Sentences}

Clotho provides one function for generating random sentences as an example of
how to use these convenience functions.

@defproc[(random-ascii-sentence
          [word-bound exact-nonnegative-integer? 16]
          [word-length-bound exact-nonnegative-integer? 8])
         string?]{
Returns a random string of words separated by spaces.
Each word is produced by calling @racket[random-ascii-word-string], and the
words are then joined.

The implementation of this function is:

@racketblock[
(define (random-ascii-sentence [word-bound 16]
                               [word-length-bound 8])
  (string-join
   (for/list ([i (in-range (random 1 word-bound))])
     (random-ascii-word-string (random 1 word-length-bound)))))
]
}

@section{Macros}

Clotho provides a few macros that make it easier to write code that relies on
randomness functionality in certain circumstances.

@subsection{Using Racket's Randomness Functions}

If you need to call a function which makes use of Racket's built-in randomness
functions but want for the values generated to be parameterized, Clotho
provides a macro:

@defform[(wrap-external-randomness
           [#:prg prg:expr]
           [#:seed seed:expr]
           body ...+)]{
Installs a new @racket[current-pseudo-random-generator] so that functions using
the non-Clotho randomness functions will be handled with Clotho's mechanisms.
Most uses will not require the additional parameters.

If given a @racket[#:prg] argument, that @racket[prg] will be installed as the
new @racket[current-pseudo-random-generator].
If instead given a @racket[#:seed] argument, a completely clean
@racket[pseudo-random-generator?] will be created and installed as the
@racket[current-pseudo-random-generator], and then seeded by calling
@racket[(random-seed seed)].
Otherwise, a new @racket[pseudo-random-generator?] is created and seeded with
the next @racket[random-seed-value].
}

@defform[(require-clotho-wrapped module-name [#:provide? provide:bool])]{

Must be used in a position where @racket[require] is valid.
Requires the phase-0 exports of @racket[module-name] and automatically wraps them with @racket[wrap-external-randomness].
Useful for wrapping libraries of random functions.

If given @racket[#t] for the @racket[#:provide?] argument, the wrapped identifiers will also be @racket[provide]d.

@racketblock[
(require clotho)

(require-clotho-wrapped math/distributions)
(code:comment "These functions from math/distributions now use")
(code:comment "Clotho's parametric randomness.")
(define d (normal-dist))
(define xs (sample d 100))
]

}

@subsection{Randomness Abstraction}

Sometimes, it may be beneficial to create regions of abstracted randomness.
This can allow for controlling randomness with a less-fine grain.
To support this, Clotho supplies a macro:

@defform[(with-new-random-source
           [#:random-source source:expr]
           [#:seed seed:expr]
           body ...+)]{
Installs a new @racket[random-source?] so that the
@racket[current-random-source] is not used for the @racket[body] forms.
Most uses will not require the additional parameters.

If given a @racket[#:random-source] argument, that @racket[source] will be
installed as the new @racket[current-random-source].
If instead given a @racket[#:seed] argument, a new @racket[random-source?] will
be created by calling @racket[(make-random-source seed)] and then installed as
the @racket[current-random-source].
Otherwise, a new @racket[random-source?] is created using the next
@racket[random-seed-value] from the @racket[current-random-source].
}


@section{Version String}
Programs that want to re-run executions with the same randomness source probably want to be reproducible.
But reproducibility usually doesn't cross version boundaries.
You might not only want to put your own program's version information somewhere in the output, but also the version of Clotho.

@defthing[clotho-version-string string?]{
A string containing the version info for Clotho.
}

@section{Stateful Execution}

@defmodule[clotho/stateful]

By default, Clotho requires you to @racket[parameterize] the
@racket[current-random-source] to use any of the included randomness functions.
However, this can be a pain to deal with in some contexts, such as during a
command-line REPL session.

Stateful (i.e., non-parameterized) randomness can be achieved by using the
@racket[clotho/stateful] module.
This module automatically instantiates a @racket[current-random-source], and
also allows for side-effecting manipulation of that @racket[random-source?]
without the need for any @racket[parameterize] forms.

Two new functions are provided by @racket[clotho/stateful]:

@defproc[(initialize-current-random-source! [val any/c #f]) void?]{
Initialize the @racket[current-random-source] with a new
@racket[random-source?].
The optional @racket[val] must conform to the specifications of
@racket[make-random-source].
}

@defproc[(uninitialize-current-random-source!) void?]{
Destroy the @racket[current-random-source].
}

@section{Avoiding Racket's Built-In Functions}

@defmodule[clotho/racket/base]

Some people may like static guarantees that they are not accidentally calling
Racket's built-in randomness functions when they intended to use Clotho's.
To aid in this goal, Clotho provides
@racket[clotho/racket/base].

This module provides everything defined in @racket[racket/base], except it
leaves out the following procedures:

@itemlist[
  @item{@racket[racket/base:random]}
  @item{@racket[racket/base:random-seed]}
  @item{@racket[racket/base:make-pseudo-random-generator]}
  @item{@racket[racket/base:pseudo-random-generator?]}
  @item{@racket[racket/base:current-pseudo-random-generator]}
  @item{@racket[racket/base:pseudo-random-generator->vector]}
  @item{@racket[racket/base:vector->pseudo-random-generator]}
  @item{@racket[racket/base:vector->pseudo-random-generator!]}
  @item{@racket[racket/base:pseudo-random-generator-vector?]}
]

The @racket[clotho/racket/base] module can also be used as a
@hash-lang[] for convenience.  Note that @racket[require]-ing the
@racket[clotho/racket/base] module inside a module written in
@hash-lang[] @racket[racket/base] will not stop you from using those
procedures, so using @racket[clotho/racket/base] as a @hash-lang[] is
recommended.



@;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

@; End of file.
