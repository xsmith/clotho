# Clotho

**A library for parametric randomness in Racket applications**

## About Clotho

Clotho is a library for supporting parametric randomness in your Racket
applications. More information about Clotho's purpose and usage, as well as
what *parametric randomness* is, is available in the current official version
of [Clotho's guide](https://docs.racket-lang.org/clotho/index.html). A version
of this guide is also available locally when you [install
Clotho](#documentation), which will be accurate relative to your local copy of
the library.

Clotho was developed at the [University of Utah School of
Computing](https://www.cs.utah.edu/) by:

- Pierce Darragh &lt;pierce.darragh@gmail.com&gt;
- William Gallard Hatch &lt;william@hatch.uno&gt;
- Eric Eide &lt;eeide@cs.utah.edu&gt;

## Installation

The easiest way to install the current release of Clotho is to use the
[official Racket package catalog](https://pkgs.racket-lang.org/):

    $ raco pkg install clotho

Alternatively, you can install the most current version of Clotho from source:

    $ git clone https://gitlab.flux.utah.edu/xsmith/clotho.git
    $ cd clotho
    $ make

Note that the version obtained from GitLab is likely to be out of sync with the
release version available from the official Racket package catalog. For most
use cases, we recommend the release from the official package catalog.

## Documentation

Clotho's documentation is built on your local machine as part of the
[installation](#installation) process and can be accessed by doing:

    $ raco docs clotho

You can also access the documentation for the current release of Clotho [via
the online Racket
documentation](https://docs.racket-lang.org/clotho/index.html).

## Reporting Bugs

Please submit bug reports via [Clotho's GitLab project issue
tracker](https://gitlab.flux.utah.edu/xsmith/clotho/-/issues).

## Acknowledgments

This material is based upon work supported by the National Science Foundation
under Grant Number 1527638. Any opinions, findings, and conclusions or
recommendations expressed in this material are those of the author(s) and do
not necessarily reflect the views of the National Science Foundation.
